package com.realdolmen.jcc;

import com.realdolmen.jcc.commands.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CommandRegistry {
    private static final MemoryFileManager defaultFileManager = new MemoryFileManager();
    public static final Map<String, Function<List<String>, ? extends Command>> commandFactories = initCommands();
    private static final Command NOOP = new Command(null, null) {
        @Override
        public void eval() {
            // no op
        }
    };

    private CommandRegistry() {/* can't be instantiated from outside*/}

    private static Map<String, Function<List<String>, ? extends Command>> initCommands() {
        Map<String, Function<List<String>, ? extends Command>> cmdReg = new HashMap<>();
        cmdReg.put("ls", args -> new LsCommand(defaultFileManager));
        cmdReg.put("cd", args -> new CdCommand(defaultFileManager, args.get(0)));
        cmdReg.put("mkdir", args -> new MkDirCommand(defaultFileManager, args.get(0)));
        cmdReg.put("more", args -> new MoreCommand(defaultFileManager, args.get(0)));
        cmdReg.put("append", args -> new AppendCommand(defaultFileManager, args.toArray(new String[]{})));
        cmdReg.put("pwd", args -> new PwdCommand(defaultFileManager));
        cmdReg.put("touch", args -> new TouchCommand(defaultFileManager, args.get(0)));
        return cmdReg;
    }

    public static Command parse(String[] input) {
        if (input.length == 0) {
            return NOOP;
        }
        List<String> argsList = Arrays.stream(input)
                                      .skip(1)
                                      .collect(Collectors.toList());
        return commandFactories.get(input[0])
                               .apply(argsList);
    }
}
