package com.realdolmen.jcc;

import com.realdolmen.jcc.commands.Command;

import java.util.Scanner;

public class Main {

    private static final String STOP = "q";

    public static void main(String[] args) {
        System.out.println("CLI loaded (enter q to quit):");
        try (Scanner scanner = new Scanner(System.in)) {
            String command = "";
            while (!STOP.equals(command = scanner.nextLine())) {
                String[] commandAndArgs = command.split("\\s+");
                Command c = CommandRegistry.parse(commandAndArgs);
                c.eval();
            }
        }
    }
}
