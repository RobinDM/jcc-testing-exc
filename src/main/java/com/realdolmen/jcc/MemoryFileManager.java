package com.realdolmen.jcc;

import com.realdolmen.jcc.adts.FileManager;
import com.realdolmen.jcc.adts.JccFile;
import com.realdolmen.jcc.adts.JccFileBase;
import com.realdolmen.jcc.adts.JccFolder;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MemoryFileManager implements FileManager {
    private static final String MOVE_UP = "..";

    private final JccFolder root = new JccFolder("", null);

    private JccFolder currentWorkingDirectory = root;

    public List<String> listContents() {
        return currentWorkingDirectory.getChildren()
                                      .stream()
                                      .map(JccFileBase::getName)
                                      .collect(Collectors.toList());
    }

    public String changeWorkingDirectoryTo(String newPath) {
        JccFolder dir = newPath.startsWith(File.separator) ? root : currentWorkingDirectory;
        String[] steps = newPath.split(File.separator);
        for (String step : steps) {
            if (MOVE_UP.equals(step)) {
                dir = dir.getParent();
            } else {
                // "checked" cast, but still very bad!
                dir = (JccFolder) dir.getChildren()
                                     .stream()
                                     .filter(JccFileBase::isDirectory)
                                     .filter(f -> step.equals(f.getName()))
                                     .findFirst()
                                     .get();
            }
        }
        currentWorkingDirectory = dir;
        return currentWorkingDirectory.absolutePath();
    }

    public String getWorkingDirectory() {
        return currentWorkingDirectory.absolutePath();
    }

    /**
     * Please make me return void. I only return boolean for the sake of testing stubbing with mockito!
     *
     * @param name
     * @return
     */
    public boolean makeNewDirectory(String name) {
        try {
            currentWorkingDirectory.addChildDirectory(name);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void createNewFile(String name) {
        currentWorkingDirectory.addChildFile(name);
    }

    public List<String> showContentsOfFile(String file) {
        Optional<JccFileBase> optionalFile = currentWorkingDirectory.getChildren()
                                                                    .stream()
                                                                    .filter(f -> !f.isDirectory())
                                                                    .filter(f -> file.equals(f.getName()))
                                                                    .findFirst();
        return ((JccFile) optionalFile.get()).getContents();
    }

    public void appendToFile(String file, String... toAppend) {
        Optional<JccFileBase> optionalFile = currentWorkingDirectory.getChildren()
                                                                    .stream()
                                                                    .filter(f -> !f.isDirectory())
                                                                    .filter(f -> file.equals(f.getName()))
                                                                    .findFirst();
        optionalFile.ifPresent(f -> ((JccFile) f).getContents()
                                                 .addAll(Arrays.asList(toAppend)));
    }


}
