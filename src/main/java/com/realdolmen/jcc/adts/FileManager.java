package com.realdolmen.jcc.adts;

import java.util.List;

public interface FileManager {
    List<String> listContents();

    String changeWorkingDirectoryTo(String newPath);

    String getWorkingDirectory();

    /**
     * Please make me return void. I only return boolean for the sake of testing stubbing with mockito!
     *
     * @param name
     * @return
     */
    boolean makeNewDirectory(String name);

    void createNewFile(String name);

    List<String> showContentsOfFile(String file);

    void appendToFile(String file, String... toAppend);

}
