package com.realdolmen.jcc.adts;

import java.util.ArrayList;
import java.util.List;

public class JccFile extends JccFileBase {
    private final List<String> contents = new ArrayList<>();

    public JccFile(String name, JccFolder parent) {
        super(name, parent);
    }

    public List<String> getContents() {
        return contents;
    }

    @Override
    public boolean isDirectory() {
        return false;
    }
}
