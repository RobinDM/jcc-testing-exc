package com.realdolmen.jcc.adts;

public abstract class JccFileBase {
    private String name;
    private JccFolder parent;

    public JccFileBase(String name, JccFolder parent) {
        this.name = name;
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public JccFolder getParent() {
        return parent;
    }

    public abstract boolean isDirectory();
}
