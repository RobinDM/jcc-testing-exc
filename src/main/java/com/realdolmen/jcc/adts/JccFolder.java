package com.realdolmen.jcc.adts;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class JccFolder extends JccFileBase {
    private final List<JccFileBase> children = new ArrayList<>();

    public JccFolder(
            String name, JccFolder parent
    ) {
        super(name, parent);
    }

    public void addChildDirectory(String childDirName) {
        children.add(new JccFolder(childDirName, this));
    }

    public void addChildFile(String childFileName) {
        children.add(new JccFile(childFileName, this));
    }

    public List<JccFileBase> getChildren() {
        return children;
    }

    public String absolutePath() {
        List<String> parts = new ArrayList<>();
        JccFolder it = this;
        do {
            parts.add(0, File.separator);
            parts.add(0, it.getName());
            it = it.getParent();
        } while (it != null);
        return String.join("", parts);
    }

    @Override
    public boolean isDirectory() {
        return true;
    }
}
