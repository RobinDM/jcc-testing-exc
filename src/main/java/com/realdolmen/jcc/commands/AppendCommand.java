package com.realdolmen.jcc.commands;

import com.realdolmen.jcc.adts.FileManager;

import java.util.Arrays;

public class AppendCommand extends Command {
    public AppendCommand(FileManager fileManager, String[] args) {
        super(fileManager, args);
    }

    @Override
    public void eval() {
        String[] contents = Arrays.stream(getArgs())
                                  .skip(1)
                                  .toArray(String[]::new);
        getFileManager().appendToFile(getArgs()[0], contents);
    }
}
