package com.realdolmen.jcc.commands;

import com.realdolmen.jcc.adts.FileManager;

public class CdCommand extends Command {
    public CdCommand(FileManager fileManager, String arg) {
        super(fileManager, new String[]{arg});
    }

    @Override
    public void eval() {
        System.out.println(getFileManager().changeWorkingDirectoryTo(getArgs()[0]));
    }
}
