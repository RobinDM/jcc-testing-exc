package com.realdolmen.jcc.commands;

import com.realdolmen.jcc.adts.FileManager;

public abstract class Command {
    private final FileManager fileManager;
    private final String[] args;

    protected Command(FileManager fileManager, String[] args) {
        this.fileManager = fileManager;
        this.args = args;
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    public String[] getArgs() {
        return args;
    }

    public abstract void eval();
}
