package com.realdolmen.jcc.commands;

import com.realdolmen.jcc.adts.FileManager;

import java.util.List;

public class LsCommand extends Command {
    public LsCommand(FileManager fileManager) {
        super(fileManager, null);
    }

    @Override
    public void eval() {
        List<String> contents = getFileManager().listContents();
        for (String content : contents) {
            System.out.println(content);
        }
    }
}
