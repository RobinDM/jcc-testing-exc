package com.realdolmen.jcc.commands;

import com.realdolmen.jcc.adts.FileManager;

public class MkDirCommand extends Command {
    public MkDirCommand(FileManager fileManager, String arg) {
        super(fileManager, new String[]{arg});
    }

    @Override
    public void eval() {
        getFileManager().makeNewDirectory(getArgs()[0]);
    }
}
