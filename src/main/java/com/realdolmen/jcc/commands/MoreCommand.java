package com.realdolmen.jcc.commands;

import com.realdolmen.jcc.adts.FileManager;

import java.util.List;

public class MoreCommand extends Command {
    public MoreCommand(FileManager fileManager, String arg) {
        super(fileManager, new String[]{arg});
    }

    @Override
    public void eval() {
        List<String> strings = getFileManager().showContentsOfFile(getArgs()[0]);
        System.out.println("Contents of " + getArgs()[0]);
        for (String string : strings) {
            System.out.println(string);
        }
    }
}
