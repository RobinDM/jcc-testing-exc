package com.realdolmen.jcc.commands;

import com.realdolmen.jcc.adts.FileManager;

public class PwdCommand extends Command {

    public PwdCommand(FileManager fileManager) {
        super(fileManager, new String[]{});
    }

    @Override
    public void eval() {
        System.out.println(getFileManager().getWorkingDirectory());
    }
}
