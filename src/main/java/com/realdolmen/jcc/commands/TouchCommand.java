package com.realdolmen.jcc.commands;

import com.realdolmen.jcc.adts.FileManager;

public class TouchCommand extends Command {
    public TouchCommand(FileManager fileManager, String arg) {
        super(fileManager, new String[]{arg});
    }

    @Override
    public void eval() {
        getFileManager().createNewFile(getArgs()[0]);
    }
}
