package com.realdolmen.jcc;

import com.realdolmen.jcc.adts.FileManager;
import com.realdolmen.jcc.commands.AppendCommand;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AppendCommandTest {

    @Mock
    private FileManager fileManager;

    @Test
    void eval() {
        String filePath = "/some/file.txt";
        String arg1 = "Hello";
        String arg2 = "World";
        Mockito.doNothing()
               .when(fileManager)
               .appendToFile(filePath, arg1, arg2);
        new AppendCommand(fileManager, new String[]{filePath, arg1, arg2}).eval();
        Mockito.verify(fileManager, Mockito.times(1))
               .appendToFile(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
    }
}