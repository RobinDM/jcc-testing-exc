package com.realdolmen.jcc;

import com.realdolmen.jcc.adts.FileManager;
import com.realdolmen.jcc.commands.CdCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CdCommandTest {

    @Mock
    private FileManager fileManager;

    @Test
    void eval() {
        String pwd = "/home/root";
        String newPath = "/a/b/c";
        Mockito.when(fileManager.getWorkingDirectory())
               .thenReturn(pwd);
        Mockito.when(fileManager.changeWorkingDirectoryTo(newPath))
               .thenReturn(newPath);
        ArgumentCaptor<String> newWorkingDirectoryCaptor = ArgumentCaptor.forClass(String.class);
        new CdCommand(fileManager, newPath).eval();
        Mockito.verify(fileManager, Mockito.times(1))
               .changeWorkingDirectoryTo(newWorkingDirectoryCaptor.capture());
        Assertions.assertEquals(newPath, newWorkingDirectoryCaptor.getValue());
    }
}