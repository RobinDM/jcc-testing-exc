package com.realdolmen.jcc;

import com.realdolmen.jcc.adts.FileManager;
import com.realdolmen.jcc.commands.LsCommand;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

@ExtendWith(MockitoExtension.class)
class LsCommandTest {

    @Mock
    private FileManager fileManager;

    @Test
    void evalLsCommand() {
        Mockito.when(fileManager.listContents()).thenReturn(Arrays.asList(
                "abc.txt",
                "hello.world"
        ));
        new LsCommand(fileManager).eval();
        Mockito.verify(fileManager, Mockito.times(1)).listContents();
    }
}