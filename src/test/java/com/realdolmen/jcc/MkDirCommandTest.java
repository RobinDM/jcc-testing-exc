package com.realdolmen.jcc;

import com.realdolmen.jcc.adts.FileManager;
import com.realdolmen.jcc.commands.MkDirCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MkDirCommandTest {

    @Mock
    private FileManager fileManager;

    @Test
    void eval() {
        String newDir = "ayo";
        Mockito.when(fileManager.makeNewDirectory("ayo"))
               .thenReturn(true);
        ArgumentCaptor<String> newDirectoryCaptor = ArgumentCaptor.forClass(String.class);
        new MkDirCommand(fileManager, newDir).eval();
        Mockito.verify(fileManager, Mockito.times(1))
               .makeNewDirectory(newDirectoryCaptor.capture());
        Assertions.assertEquals(newDir, newDirectoryCaptor.getValue());
    }
}