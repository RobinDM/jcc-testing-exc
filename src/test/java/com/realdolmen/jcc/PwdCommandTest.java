package com.realdolmen.jcc;

import com.realdolmen.jcc.adts.FileManager;
import com.realdolmen.jcc.commands.PwdCommand;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PwdCommandTest {

    @Mock
    private FileManager fileManager;

    @Test
    public void eval() {
        new PwdCommand(fileManager).eval();
        Mockito.verify(fileManager, Mockito.times(1))
               .getWorkingDirectory();
    }
}